/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
group = "li.void_.gradle"
// version = "unspecified"

plugins {
  kotlin("jvm") version "1.6.0"

  `maven-publish`
  signing

  id("li.void_.gradle.gradle-version" ) version "1.0.0"
  id("li.void_.gradle.gradle-versions") version "1.0.0"
}

repositories {
  mavenCentral()
}

val javaVersion = JavaVersion.VERSION_11

subprojects {
  project.group   = rootProject.group
  project.version = rootProject.version

  if (project.extensions.findByName("dep") == null) {
    project.extensions.add("dep", rootProject.dep)
  }

  apply(plugin = "kotlin")

  apply(plugin = "maven-publish")
  apply(plugin = "signing")

  repositories {
    mavenCentral()
  }

  tasks.withType<                                    JavaCompile>().all {
    sourceCompatibility     = javaVersion.toString()
    targetCompatibility     = javaVersion.toString()
  }

  tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile>().all {
    kotlinOptions.jvmTarget = javaVersion.toString()
  }

  tasks.withType<Test>().all {
    useJUnitPlatform()
  }

  publishing {
    repositories {
      maven {
        url = uri(
          String.format("%s/projects/%s/packages/maven",
            rootProject.extra["publishing_mavenapi"].toString(),
            rootProject.extra["publishing_mavenpid"].toString()
          )
        )

        authentication {
          create<HttpHeaderAuthentication>("HttpHeaderAuthentication")
        }

        credentials(HttpHeaderCredentials::class) {
          name  = rootProject.extra["publishing_username"].toString()
          value = rootProject.extra["publishing_password"].toString()
        }
      }
    }

    publications.configureEach(
      @Suppress("ObjectLiteralToLambda")
      object : Action<Publication> {
        override fun execute(publication: Publication) {
          if (rootProject.extra.has("signing.gnupg.keyName")) {
            signing.sign(publication)
          }
        }
      }
    )
  }

  if (rootProject.extra.has("signing.gnupg.keyName")) {
    signing {
      useGpgCmd()
      sign(configurations.archives.get())
    }
  }
}

tasks.register("jacocoTestReport", Copy::class) {
  val someProject     =     project(":gradle-version")
  val someProjectTask = someProject.tasks[name]

  group = someProjectTask.group!!
  dependsOn(someProjectTask)

  from("${someProject.buildDir}/reports/jacoco/test")
  into("${rootProject.buildDir}/reports/jacoco/test")

  include("jacocoTestReport.xml")
}
