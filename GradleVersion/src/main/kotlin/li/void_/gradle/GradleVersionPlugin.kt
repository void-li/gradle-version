/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
package li.void_.gradle

import org.gradle.api.Plugin
import org.gradle.api.Project
import java.io.IOException
import java.nio.charset.Charset
import java.util.concurrent.TimeUnit

@Suppress("unused")
class GradleVersionPlugin : Plugin<Project> {
  override fun apply(target: Project) {
    @Suppress("UnnecessaryVariable")
    val project = target

    project.logger.trace("{} apply '{}'",
      this::class.simpleName, project
    )

    if (project.version != "unspecified") {
      project.logger.info("${this::class.simpleName} Project version already set.")
      return
    }

    try {
      Runtime.getRuntime().exec("git")
    }
    catch (e: IOException) {
      project.logger.warn("${this::class.simpleName} Git binary unavailable.")
      return
    }

    val processBuilder = ProcessBuilder("git", "-C", project.rootProject.projectDir.path, "describe")
    val process        = processBuilder.start()

    if (!process.waitFor(1, TimeUnit.SECONDS)) {
      val messageText = process.errorStream.readAllBytes().toString(Charset.defaultCharset()).trim()
      val message     = "${this::class.simpleName} Git describe hangs: '${messageText}'."

      project.logger.warn(message)
      return
    }

    if (process.exitValue() != 0) {
      val messageText = process.errorStream.readAllBytes().toString(Charset.defaultCharset()).trim()
      val message     = "${this::class.simpleName} Git describe error: '${messageText}'."

      project.logger.warn(message)
      return
    }

    val version = process.inputStream.readAllBytes().toString(Charset.defaultCharset()).trim()
    if (version.matches(VERSION_V_REGEXPR)) {
      project.version = version.substringAfter("v")
    } else {
      project.version = version
    }
  }

  companion object {
    val VERSION_V_REGEXPR = Regex("v[0-9]+.*")
  }
}
