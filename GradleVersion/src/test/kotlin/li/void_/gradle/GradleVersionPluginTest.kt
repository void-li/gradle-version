/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
package li.void_.gradle

import org.gradle.testfixtures.ProjectBuilder
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.File
import java.nio.file.Files
import java.util.concurrent.TimeUnit

class GradleVersionPluginTest {
  private lateinit var tmpDir: File

  @BeforeEach
  fun setupTmpDir() {
    tmpDir = Files.createTempDirectory("tmp").toFile()
  }

  @BeforeEach
  fun setupRepository() {
    @Suppress("JoinDeclarationAndAssignment")
    var processBuilder: ProcessBuilder

    @Suppress("JoinDeclarationAndAssignment")
    var process       : Process

    processBuilder = ProcessBuilder("git", "-C", tmpDir.path, "init")
    process        = processBuilder.start()

    process.waitFor(1, TimeUnit.SECONDS)
    Assertions.assertFalse(process.isAlive)

    processBuilder = ProcessBuilder("git", "-C", tmpDir.path, "commit", "--allow-empty", "-m", "Message"          )
    process        = processBuilder.start()

    process.waitFor(1, TimeUnit.SECONDS)
    Assertions.assertFalse(process.isAlive)

    processBuilder = ProcessBuilder("git", "-C", tmpDir.path, "tag",     "-a",           "-m", "Message", "v1.0.0")
    process        = processBuilder.start()

    process.waitFor(1, TimeUnit.SECONDS)
    Assertions.assertFalse(process.isAlive)
  }

  @Test
  fun extractVersionFromGitTag() {
    val project = ProjectBuilder.builder().withProjectDir(tmpDir).build()
  //project.version = "1.2.3"

    project.plugins.apply(PLUGIN_ID)

    Assertions.assertEquals("1.0.0",
      project.version
    )
  }

  @Test
  fun extractVersionFromGitTagWithoutVPrefix() {
    @Suppress("JoinDeclarationAndAssignment")
    var processBuilder: ProcessBuilder

    @Suppress("JoinDeclarationAndAssignment")
    var process       : Process

    processBuilder = ProcessBuilder("git", "-C", tmpDir.path, "tag", "-d",                 "v1.0.0")
    process        = processBuilder.start()

    process.waitFor(1, TimeUnit.SECONDS)
    Assertions.assertFalse(process.isAlive)

    processBuilder = ProcessBuilder("git", "-C", tmpDir.path, "tag", "-a", "-m", "Message", "1.0.0")
    process        = processBuilder.start()

    process.waitFor(1, TimeUnit.SECONDS)
    Assertions.assertFalse(process.isAlive)

    val project = ProjectBuilder.builder().withProjectDir(tmpDir).build()
  //project.version = "1.2.3"

    project.plugins.apply(PLUGIN_ID)

    Assertions.assertEquals("1.0.0",
      project.version
    )
  }

  @Test
  fun extractVersionFromGradle() {
    val project = ProjectBuilder.builder().withProjectDir(tmpDir).build()
    project.version = "1.2.3"

    project.plugins.apply(PLUGIN_ID)

    Assertions.assertEquals("1.2.3",
      project.version
    )
  }

  @Test
  fun extractVersionUnavailable() {
    val processBuilder = ProcessBuilder("git", "-C", tmpDir.path, "tag", "-d", "v1.0.0")
    val process        = processBuilder.start()

    process.waitFor(1, TimeUnit.SECONDS)
    Assertions.assertFalse(process.isAlive)

    val project = ProjectBuilder.builder().withProjectDir(tmpDir).build()
  //project.version = "1.2.3"

    project.plugins.apply(PLUGIN_ID)

    Assertions.assertEquals("unspecified",
      project.version
    )
  }

  @AfterEach
  fun closeTmpDir() {
    if (tmpDir.exists()) {
      tmpDir.delete()
    }
  }

  companion object {
    const val PLUGIN_ID = "li.void_.gradle.gradle-version"
  }
}
