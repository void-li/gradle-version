/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */

plugins {
  kotlin("jvm")

  java
  `java-gradle-plugin`
  jacoco
}

dependencies {
  @Suppress("GradlePackageUpdate")
  implementation(kotlin("stdlib"))

  testImplementation(dep("org.junit.jupiter", "junit-jupiter-api"   ))
  testRuntimeOnly   (dep("org.junit.jupiter", "junit-jupiter-engine"))
}

tasks.jacocoTestReport {
  dependsOn(tasks.test)

  reports {
    xml.required.set(true)
  }
}

gradlePlugin {
  plugins {
    create("GradleVersionPlugin") {
      id                  = "$group.gradle-version"
      implementationClass = "$group.GradleVersionPlugin"
    }
  }
}
